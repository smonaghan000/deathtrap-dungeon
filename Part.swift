//
//  Part.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 7/14/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class Part {
  let id: Int
  var contents:[Content] = []
  var entities:[Item] = []
  var actions:[Action] = []
  private var _router:Router?
  
  init(_ id:Int){
    self.id = id
  }
  
  var router:Router? {
    get {
      return self._router
    }
    set(router){
      self._router = router
    }
  }
}
