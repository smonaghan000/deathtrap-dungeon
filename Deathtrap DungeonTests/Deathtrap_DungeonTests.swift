//
//  Deathtrap_DungeonTests.swift
//  Deathtrap DungeonTests
//
//  Created by Sean Monaghan on 7/14/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import XCTest
import Foundation
@testable import Deathtrap_Dungeon


class Deathtrap_DungeonTests: XCTestCase {
    
    override func setUp () {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown () {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
  func testPartInitialization () {
    let part = Part(0)
    part.contents = [Content(text:"Content A"), Content(text:"Content B")]
    let routes = [Route(to:1, text:"Go to 1")]
    part.router = Router(RouterType.choice, routes:routes)
    XCTAssertNotNil(part)
    XCTAssertTrue(part.contents.count == 2)
  }
  
  func testRouteInitialization () {
    let route = Route(to:1, text:"If you would like a sandwich, go to 1.")
    XCTAssertNotNil(route)
    XCTAssertEqual(1, route.to)
    XCTAssertEqual("If you would like a sandwich, go to 1.", route.text)
  }
  
  func testLuckRouteInitialization () {
    let route = LuckRoute(to:1, condition:LuckRoute.Condition.lucky, text:"Go to 1 if you are lucky.")
    XCTAssertNotNil(route)
    XCTAssertEqual(1, route.to)
    XCTAssertEqual("Go to 1 if you are lucky.", route.text)
    XCTAssertEqual(LuckRoute.Condition.lucky, route.condition)
  }
  
  func testInventoryRouteInitialization () {
    let sword = Item(id:"sword", label:"sword")
    let route = InventoryRoute(to:1, condition:InventoryRoute.Condition.has, item:sword, text:"Go to 1 if you have a sword.")
    XCTAssertNotNil(route)
    XCTAssertEqual(1, route.to)
    XCTAssertEqual("Go to 1 if you have a sword.", route.text)
    XCTAssertEqual(sword.label, route.item.label)
    XCTAssertEqual(InventoryRoute.Condition.has, route.condition)
  }
  
  func testSimplePartParser () {
    let path = "/Users/smonaghan/Development/sandbox/Deathtrap Dungeon/data/story/parts/1.xml"
    let url = URL(fileURLWithPath:path)
    let parser = PartParser()
    parser.parse(url)
    
    XCTAssertNotNil(parser.currentPart)
    
    if let part = parser.currentPart {
      XCTAssertEqual(1, part.id)
      XCTAssertEqual(2, part.contents.count)
      XCTAssertEqual(RouterType.choice, part.router?.type)
    }
  }
  
  func testInventoryPartParser () {
    let path = "/Users/smonaghan/Development/sandbox/Deathtrap Dungeon/data/story/parts/10.xml"
    let url = URL(fileURLWithPath:path)
    let parser = PartParser()
    parser.parse(url)
    
    XCTAssertNotNil(parser.currentPart)
    
    if let part = parser.currentPart {
      XCTAssertEqual(1, part.contents.count)
      XCTAssertEqual(RouterType.inventory, part.router?.type)
    }
  }
  
  func testWorldLoader () {
    let path = "/Users/smonaghan/Development/sandbox/Deathtrap Dungeon/data/story/parts"
    let url = URL(fileURLWithPath:path)
    let world = World()
    world.load(url)
    XCTAssertTrue(world.parts.count > 0)
  }
  
  func testDieRoll () {
    let sixSided = Die()
    let roll = sixSided.roll()
    print("Six Sided Rolls \(roll)")
    XCTAssertTrue(roll > 0)
  }
  
  func testDiceRole () {
    let sixSided = Die()
    let twelveSided = Die(12)
    let dice = Dice([sixSided, twelveSided])
    let roll = dice.roll()
    print("Dice Roll \(roll)")
    XCTAssertTrue(roll > 0)
  }
  
  func testPlayerLuck () {
    let dieA = Die()
    let dieB = Die()
    let dice = Dice([dieA, dieB])
    
    let skill = dieA.roll() + 6
    let stamina = dice.roll() + 12
    let luck = dieA.roll() + 6
    
    let player = Player(name: "Test Player", skill:skill, stamina:stamina, luck:luck, provisions:Provisions(), inventory:Inventory())
    
    if player.testLuck(dice) {
        XCTAssertTrue(player.luck == luck-1)
    }
  }
}
