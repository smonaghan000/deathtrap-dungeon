//
//  PropertyResetAction.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 8/8/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class PropertyResetAction : Action {
  
  let property:Player.Property
  
  init(property:Player.Property){
    self.property = property
  }
  
  func apply(_ player: Player) {
    switch property {
    case Player.Property.skill :
      player.skill = 0
    case Player.Property.stamina :
      player.stamina = 0
    case Player.Property.luck :
      player.luck = 0
    default: ()
    }
  }
  
}
