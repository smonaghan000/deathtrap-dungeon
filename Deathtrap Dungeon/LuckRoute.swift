//
//  LuckRoute.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 7/28/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class LuckRoute : Route {
  
  enum Condition {
    case lucky
    case unlucky
    
    static func byName(_ name:String) -> Condition? {
      switch name {
      case "lucky":
        return Condition.lucky
      case "unlucky":
        return Condition.unlucky
      default:
        return Optional.none
      }
    }
  }
  
  var condition:Condition
  
  init (to:Int, condition:Condition, text:String = "No Text") {
    self.condition = condition
    super.init(to:to, text:text)
  }
}
