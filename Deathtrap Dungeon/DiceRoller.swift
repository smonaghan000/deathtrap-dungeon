//
//  Die.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 8/4/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class Dice {

  let dice:[Die]  
  
  init(_ dice:[Die] = [Die(),Die()]){
    self.dice = dice
  }
  
  func roll() -> Int {
    return dice.reduce(0,{$0 + $1.roll()})
  }
  
  func reset() {
    dice.forEach({$0.reset()})
  }
  
  var currentValue:Int {
    get {
      return dice.reduce(0,{$0 + $1.currentValue})
    }
  }
  
}

class Die {

  let sides:Int
  private var _currentValue:Int = 0
  
  init(_ sides:Int = 6){
    self.sides = sides
  }
  
  func roll() -> Int {
    _currentValue = Int(arc4random_uniform(UInt32(self.sides)) + 1)
    return _currentValue
  }
  
  func reset() {
   _currentValue = 0
  }
  
  var currentValue:Int {
    get {
      return self._currentValue
    }
  }
}
