//
//  RollRoute.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 8/5/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class RollRoute : Route {

  enum Condition {
    case lte
    case gte
    
    static func byName(_ name:String) -> ((Int , Int) -> Bool)? {
      switch name {
      case "lte":
        return {$0 <= $1}
      case "gte":
        return {$0 >= $1}
      default:
        return Optional.none
      }
    }
  }
  
  let dice:Dice
  let condition:(Int , Int) -> Bool
  let property:Player.Property
  
  init(to:Int, dice:Dice, condition:@escaping (Int , Int) -> Bool, property:Player.Property, text:String = "No Text") {
    self.dice = dice
    self.condition = condition
    self.property = property
    super.init(to:to, text:text)
  }
  
}
