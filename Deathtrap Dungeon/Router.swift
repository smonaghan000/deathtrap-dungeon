//
//  PartNavigation.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 7/17/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

enum RouterType {
  case choice // user selects desired route
  case luck // route is determined based on luck roll
  case victory // route is determined by outcome of battle
  case inventory // route is determined by current state of inventory
  case roll // route is determined by dice roll
  case unknown // route is of an unknown type
  
  static func byName(_ name:String) -> RouterType{
    switch name {
    case "choice":
      return choice
    case "luck":
      return luck
    case "victory":
      return victory
    case "inventory":
      return inventory
    case "roll":
      return roll
    default:
      return unknown
    }
  }
}

class Router {
  
  var type:RouterType  
  var routes:[Route]
  
  init(_ type:RouterType, routes:[Route] = []){
    self.type = type
    self.routes = routes
  }
  
}
