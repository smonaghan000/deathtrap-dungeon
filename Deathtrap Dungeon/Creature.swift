//
//  Creature.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 8/2/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class Creature {

  let name:String
  private var _skill:Int
  private var _stamina:Int
  
  init(name:String, skill:Int, stamina:Int) {
    self.name = name
    self._skill = skill
    self._stamina = stamina
  }
  
  /////////////////////////
  // PROPERTIES
  /////////////////////////
  
  var skill:Int {
    get {
      return self._skill
    }
    set(skill){
      self._skill = skill
    }
  }
  
  var stamina:Int {
    get {
      return self._stamina
    }
    set(stamina){
      self._stamina = stamina
    }
  }
  
  /////////////////////////
  // METHODS
  /////////////////////////
  
  func calculateAttackStrength(seed:Int) -> Int{
    return seed + self._skill
  }
  
  func takeDamage(damage:Int = 2){
    self._stamina -= damage
  }
  
  func alive() -> Bool {
    return self._stamina < 1
  }
}
