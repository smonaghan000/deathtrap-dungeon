//
//  World.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 8/2/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class World {

  var parts:[Part] = []
    
  init(){}
  
  // NOTE: this load is just for getting things off the ground with the data format. We should consider
  // using this load as the core data bootstrap, and then using the sqlite backend as the repository for
  // all parts, items, creatures etc, after the game initializes. Once we do that, this World class would
  // then only host a single Part - the current one, a Player, a Compass(maybe) denoting the current 
  // oritentaion of the Player... maybe Dice? 
  
  func load(_ partsDataDir:URL){
    do {
      let partURLs = try FileManager.default.contentsOfDirectory(at: partsDataDir,includingPropertiesForKeys: nil, options:[FileManager.DirectoryEnumerationOptions.skipsHiddenFiles])
      for url in partURLs {
        let parser = PartParser()
        parser.parse(url)
        if let part = parser.currentPart{
          parts.append(part)
        }
      }
    }catch{
      print("An error occurred finding part files \(error)")
    }
  }
}
