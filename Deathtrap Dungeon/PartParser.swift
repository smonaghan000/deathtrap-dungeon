//
//  PartParser.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 7/17/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class PartParser : NSObject, XMLParserDelegate {
  
  enum Mode {
    case NONE
    case PART
    case CONTENT
    case ACTION
    case ROUTER
    case ROUTE
  }
  
  var currentPart:Part?
  var currentRoute:Route?
  var currentMode:Mode = Mode.NONE
  
  public func parse(_ url:URL) {
    let parser = XMLParser(contentsOf:url)!
    parser.delegate = self
    parser.parse()
  }
  
  func parserDidStartDocument(_ parser:XMLParser) {
    print("parserDidStartDocument")
  }
  
  func parserDidEndDocument(_ parser:XMLParser) {
    print("parserDidEndDocument")
  }
  
  func parser(_ parser:XMLParser, didStartElement: String, namespaceURI: String?, qualifiedName: String?, attributes: [String : String] = [:]) {
    switch didStartElement {
    case "part":
      if let pid = Int(attributes["id"]!) {
        currentMode = Mode.PART
        currentPart = Part(pid)
        print("Initialized new Part with id \(pid)")
        currentMode = Mode.PART
      }
    case "content":
      currentMode = Mode.CONTENT
    case "router":
      currentMode = Mode.ROUTER
      if let part = currentPart, let type = attributes["type"]{
        let routerType = RouterType.byName(type)
        part.router = Router(routerType)
      }
    case "route":
      currentMode = Mode.ROUTE
      if let router = currentPart?.router{
        switch router.type {
          case RouterType.choice, RouterType.victory:
            currentRoute = assembleStandardRoute(attributes)
          case RouterType.inventory:
            currentRoute = assembleInventoryRoute(attributes)
          case RouterType.luck:
            currentRoute = assembleLuckRoute(attributes)
        case RouterType.roll:
          currentRoute = assembleRollRoute(attributes)
          default:
            print("Ignoring RouterType \(router.type)")
          }
        
        if currentRoute != nil {
          router.routes.append(currentRoute!)
        }
      }
    case "action" :
        currentMode = Mode.ACTION
      // TODO: we need to figure out how to init the various kinds of actions, based on....
    default:
      currentMode = Mode.NONE
    }
    
    print("Mode is now \(currentMode)")
  }
  
  func parser(_ parser:XMLParser, foundCharacters: String) {
    switch currentMode {
      case Mode.CONTENT:
        if let part = currentPart{
          part.contents.append(Content(text:foundCharacters))
        }
      case Mode.ROUTE:
        if let route = currentRoute{
            route.text = foundCharacters
        }
      default:
        print("")
    }
  }
  
  func parser(_ parser:XMLParser, didEndElement: String, namespaceURI: String?, qualifiedName: String?) {
    currentMode = Mode.NONE
    print("Mode is now \(currentMode)")
  }
  
  func assembleStandardRoute(_ attributes: [String : String]) -> Route? {
    if let to = Int(attributes["to"]!){
      return Route(to:to)
    }else{
      return Optional.none
    }
  }
  
  func assembleInventoryRoute(_ attributes: [String : String]) -> InventoryRoute? {
    if let to = Int(attributes["to"]!), let condition = InventoryRoute.Condition.byName(attributes["condition"]!), let itemName = attributes["item"]{
      return InventoryRoute(to:to, condition:condition, item:Item(id:itemName, label:itemName))
    }else{
      return Optional.none
    }
  }
  
  func assembleLuckRoute(_ attributes: [String : String]) -> LuckRoute? {
    if let to = Int(attributes["to"]!), let condition = LuckRoute.Condition.byName(attributes["condition"]!){
      return LuckRoute(to:to, condition:condition)
    }else{
      return Optional.none
    }
  }
  
  func assembleRollRoute(_ attributes: [String : String]) -> Route? {
    if let toAtt = attributes["to"], let conditionAttr = attributes["condition"], let propertyAttr = attributes["property"] {
      if let to = Int(toAtt) {
        if let condition = RollRoute.Condition.byName(conditionAttr), let property = Player.Property.byName(propertyAttr) {
          // TODO: consider the "dice" attribute and assemble a Dice instance containing the number of dice specified
          // NOTE: for now were just moving forward with the default of 2
          return RollRoute(to:to, dice:Dice(), condition:condition, property:property)
        }else{
          return Route(to:to)
        }
      }else{
        return Optional.none
      }
    }
    return Optional.none
  }
}
