//
//  Action.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 8/6/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class ActionLegacy {

  enum Command {
    case aquire // indicates that the target property should aquire value
    case reset // indicates that the target property should be reset to an empty value
    
    static func byName(_ name:String) -> Command? {
      switch name {
      case "aquire" :
        return Command.aquire
      case "reset" :
        return Command.reset
      default :
        return Optional.none
      }
    }
  }
  
  enum Mode {
    case auto // indicates that the command should affect the target property automatically
    case roll // indicates that the command should affect the target property according to result of a dice roll
    
    static func byName(_ name:String) -> Mode? {
      switch name {
      case "auto" :
        return Mode.auto
      case "roll" :
        return Mode.roll
      default:
        return Optional.none
      }
    }
  }
  
  let property:Player.Property
  let mode:Mode
  let value:Int
  
  init(property:Player.Property, mode:Mode, command:Command, value:Int = 0){
    self.property = property
    self.mode = mode
    self.value = value
  }
}
