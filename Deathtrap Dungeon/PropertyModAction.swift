//
//  PropertyModAction.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 8/8/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class PropertyModAction : Action {

  let property:Player.Property
  let value:Int
  
  init(property:Player.Property, value:Int){
    self.property = property
    self.value = value
  }
  
  func apply(_ player: Player) {
    switch property {
      case Player.Property.skill :
        player.skill += value
      case Player.Property.stamina :
        player.stamina += value
      case Player.Property.luck :
        player.luck += value
      default: ()
    }
  }
  
}
