//
//  Potion.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 8/4/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class Potion {

  enum PotionType {
    case skill
    case strength
    case fortune
  }

  let type:PotionType
  
  private var _available:Bool = true
  
  init(_ type:PotionType){
    self.type = type
  }
  
  var available:Bool{
    get{
      return _available
    }
  }
  
  func use(){
    _available = false
  }
  
  
}
