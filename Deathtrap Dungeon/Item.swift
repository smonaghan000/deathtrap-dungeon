//
//  Entity.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 7/17/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class Item {
  let id:String
  let label:String
  init (id:String, label:String) {
    self.id = id
    self.label = label
  }
}
