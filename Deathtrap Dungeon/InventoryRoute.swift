//
//  InventoryRoute.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 7/28/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class InventoryRoute : Route {

  enum Condition {
    case has
    case lacks
    case unknown
    
    static func byName(_ name:String) -> Condition? {
      switch name {
      case "has":
        return Condition.has
      case "lacks":
        return Condition.lacks
      default:
        return Optional.none
      }
    }
  }
  
  var condition:Condition
  var item:Item
  
  init (to:Int, condition:Condition, item:Item, text:String = "No text") {
    self.condition = condition
    self.item = item
    super.init(to:to, text:text)
  }
  

}
