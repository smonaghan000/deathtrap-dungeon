//
//  Provisions.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 8/4/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class Provisions {

  // TODO: replace Item with a more appropriate type
  var entries:[Item] = []

  init(){} 
  
  /////////////////////////
  // METHODS
  /////////////////////////
  
  func removeOne(){
    if let removed = entries.popLast() {
      // TODO: what do we want to do here?
      print("Removed \(removed.label) from provisions")
    }
  }
  
  func removeAll(){
    entries.removeAll()
  }
}
