//
//  Route.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 7/28/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class Route {
  
  var to:Int
  var text:String
  
  init (to:Int, text:String = "No Text") {
    self.to = to
    self.text = text
  }
  
}
