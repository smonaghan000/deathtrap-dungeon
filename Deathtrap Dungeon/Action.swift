//
//  Action.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 8/8/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

protocol Action {
  func apply(_ player:Player)
}
