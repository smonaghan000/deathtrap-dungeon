//
//  Content.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 7/29/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class Content {

  var text:String
  
  init (text:String) {
    
    assert(!text.isEmpty, "text must not be empty")
    
    self.text = text
  }
}
