//
//  Player.swift
//  Deathtrap Dungeon
//
//  Created by Sean Monaghan on 8/2/17.
//  Copyright © 2017 Sean Monaghan. All rights reserved.
//

import Foundation

class Player : Creature {
  
  enum Property {
    case skill
    case stamina
    case luck
    case fortune
    case provisions
    case inventory
    
    static func byName(_ name:String) -> Property? {
      switch name {
      case "skill":
        return Property.skill
      case "stamina":
        return Property.stamina
      case "luck":
        return Property.luck
      case "provisions":
        return Property.provisions
      case "inventory":
        return Property.inventory
      default:
        return Optional.none
      }
    }
  }
  
  private let _initialSkill:Int
  private let _initialStamina:Int
  private var _initialLuck:Int
  
  var luck:Int
  var provisions:Provisions
  var inventory:Inventory
  var fortune:Int = 0
  
  init(name:String, skill:Int, stamina:Int, luck:Int, provisions:Provisions, inventory:Inventory) {
    self._initialSkill = skill
    self._initialStamina = stamina
    self._initialLuck = luck
    self.luck = luck
    self.provisions = provisions
    self.inventory = inventory
    super.init(name:name, skill:skill, stamina: stamina)
  }
  
  /////////////////////////
  // PROPERTIES
  /////////////////////////
  
  override var skill:Int {
    get{
      return super.skill
    }
    
    set(skill){
      if(skill <= self._initialSkill){
        super.skill = skill
      }else{
        super.skill = self._initialSkill
      }
    }
  }
  
  override var stamina:Int {
    get {
      return super.stamina
    }
    set(stamina){
      if(stamina <= self._initialStamina){
        super.stamina = stamina
      }else{
        super.stamina = self._initialStamina
      }
    }
  }
  
  /////////////////////////
  // METHODS
  /////////////////////////
  
  func testLuck(_ dice:Dice) -> Bool {
    let lucky = dice.roll() <= luck
    luck -= 1
    return lucky
  }
    
  func applySkillPotion(potion:Potion){
    // TODO: these are not available during battle
    self.skill = self._initialSkill
  }
  
  func applyStaminaPotion(){
    // TODO: these are not available during battle
    self.stamina = self._initialStamina
  }
  
  func applyAction(action:Action) {
    action.apply(self)
  }
  
  func applyLuckPotion(){
    // TODO: these are not available during battle
    self._initialLuck = self._initialLuck + 1
    self.luck = self._initialLuck
  }
  
  func eatMeal(){
    if(provisions.entries.count > 0){
      provisions.removeOne()
    }
  }
  
}
